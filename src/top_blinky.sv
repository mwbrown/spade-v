module top(input clk, output led);
    reg [5:0] reset_cnt = 0;
    wire resetn = &reset_cnt;
    always @(posedge clk) begin
        reset_cnt <= reset_cnt + !resetn;
    end

    e_led_blink_top cpu
        ( ._i_led_blink_clk(clk)
        , ._i_led_blink_rst(!resetn)
        , .__output({led})
        );
endmodule

