`include "test/vatch/main.v"
module cpu_tb;
    `SETUP_TEST
    // 5 should flush the whole pipeline before doing anything
    `CLK_AND_RST(clk, rst, 5)

    logic[1024*32-1:0] prog;

    wire[31:0] cpu_out;
    wire cpu_out_none;


    integer current_program;
    // Load and test a program. The program is in inverted order i.e. the
    // first instruction is at the bottom.
    `define TEST_PROGRAM(PROGRAM, LOAD_TIME, RUN_TIME, OUTPUT) \
        rst = 1; \
        prog = PROGRAM; \
        repeat(5) @(negedge clk); /*Flush reset through pipeline */ \
        rst = 0; \
        repeat(LOAD_TIME) @(negedge clk); /* Wait for program load */ \
        repeat(RUN_TIME) @(negedge clk); /* Wait for program to run */ \
        `ASSERT_EQ(cpu_out_none, 0); /* Something has been outputed */ \
        `ASSERT_EQ(cpu_out, OUTPUT); /* Check output */ \
        current_program = current_program + 1;


    integer i;
    initial begin
        current_program = 0;
        @(negedge rst);
        // Register forwarding
        `TEST_PROGRAM (
            {
                32'h00000013,	 // nop
                32'h00108093,	 // addi	ra,ra,1
                32'h00108093,	 // addi	ra,ra,1
                32'h00108093,	 // addi	ra,ra,1
                32'h00000093	 // li	ra,0
            },
            6,
            20,
            3
        );

        // Register forwarding without nop at end
        `TEST_PROGRAM (
            {
                32'h00108093,	 // addi	ra,ra,1
                32'h00108093,	 // addi	ra,ra,1
                32'h00108093,	 // addi	ra,ra,1
                32'h00000093	 // li	ra,0
            },
            6,
            12,
            3
        );

        // Forward jump jumps to correct location
        `TEST_PROGRAM (
            {
                32'h00208093,	 // addi	ra,ra,2
                32'h00000013,	 // nop
                32'h00000013,	 // nop
                32'h00000013,	 // nop
                32'h00108093,	 // addi	ra,ra,1
                32'h0140006f,	 // j	20 <done>
                32'h00000013,	 // nop
                32'h00000013,	 // nop
                32'h00000093	 // li	ra,0
            },
            9,
            20,
            2
        );

        // JAL updates the link register
        `TEST_PROGRAM (
            {
                32'h00000013,	 // nop
                32'h004000ef,	 // jal	ra,8 <done>
                32'h00000093	 // li	ra,0
            },
            6,
            12,
            8
        );

        // BEQ does not take jump if non-eq operands
        `TEST_PROGRAM (
            {
                32'h00008093,	 // mv	ra,ra
                32'h00108093,	 // addi	ra,ra,1
                32'h00208463,	 // beq	ra,sp,10 <skip>
                32'h00100093,	 // li	ra,1
                32'h00000113	 // li	sp,0
            },
            6,
            20,
            2
        );

        // BEQ takes jump for eq operands
        `TEST_PROGRAM (
            {
                32'h00008093,	 // mv	ra,ra
                32'h00000013,	 // nop
                32'h00000013,	 // nop
                32'h00108093,	 // addi	ra,ra,1
                32'h00208863,	 // beq	ra,sp,18 <skip>
                32'h00100093,	 // li	ra,1
                32'h00100113	 // li	sp,1
            },
            6,
            20,
            1
        );

        // sb and lw work
        `TEST_PROGRAM (
            {
                32'h0002af03,	 // lw	t5,0(t0)
                32'h01d281a3,	 // sb	t4,3(t0)
                32'h01c28123,	 // sb	t3,2(t0)
                32'h007280a3,	 // sb	t2,1(t0)
                32'h00628023,	 // sb	t1,0(t0)
                32'h00400e93,	 // li	t4,4
                32'h00300e13,	 // li	t3,3
                32'h00200393,	 // li	t2,2
                32'h00100313,	 // li	t1,1
                32'h00000293	 // li	t0,0
            },
            9,
            20,
            'h04030201
        );

        // sh and lw work
        `TEST_PROGRAM (
            {
                32'h0002af03,	 // lw	t5,0(t0)
                32'h01c29123,	 // sh	t3,2(t0)
                32'h00629023,	 // sh	t1,0(t0)
                32'h00400e93,	 // li	t4,4
                32'h00300e13,	 // li	t3,3
                32'h00200393,	 // li	t2,2
                32'h00100313,	 // li	t1,1
                32'h00000293	 // li	t0,0
            },
            9,
            20,
            'h00030001
        );

        // Loading large constants works
        `TEST_PROGRAM (
            {
                32'h6c028293,	 // addi	t0,t0,1728 # 0x2dc6c0
                32'h002dc2b7	 // lui	t0,0x2dc
            },
            9,
            20,
            'd3000000
        );


        `END_TEST
    end

    e_cpu_test_harness uut
        ( ._i_clk(clk)
        , ._i_rst(rst)
        , ._i_program(prog)
        , .__output({cpu_out_none, cpu_out})
        );

endmodule
