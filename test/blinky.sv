
`include "test/vatch/main.v"
module blinky_tb;
    `SETUP_TEST
    // 5 should flush the whole pipeline before doing anything
    `CLK_AND_RST(clk, rst, 5)

    logic[1024*32-1:0] prog;

    wire[31:0] cpu_out;
    wire cpu_out_none;

    reg led;


    integer current_program;
    // Load and test a program. The program is in inverted order i.e. the
    // first instruction is at the bottom.
    `define TEST_PROGRAM(PROGRAM, LOAD_TIME, RUN_TIME, OUTPUT) \
        current_program = current_program + 1; \
        rst = 1; \
        prog = PROGRAM; \
        repeat(5) @(negedge clk); /*Flush reset through pipeline */ \
        rst = 0; \
        repeat(LOAD_TIME) @(negedge clk); /* Wait for program load */ \
        repeat(RUN_TIME) @(negedge clk); /* Wait for program to run */ \
        `ASSERT_EQ(cpu_out_none, 0); /* Something has been outputed */ \
        `ASSERT_EQ(cpu_out, OUTPUT); /* Check output */


    integer i;
    initial begin
        current_program = 0;
        repeat(12_000 * 4) @(negedge clk);

        `END_TEST
    end

    e_led_blink_top uut
        ( ._i_led_blink_clk(clk)
        , ._i_led_blink_rst(rst)
        , .__output(led)
        );

endmodule
