`include "test/vatch/main.v"

module decoding_tb();
    `SETUP_TEST
    `CLK_AND_RST(clk, rst, 1)

    wire[4:0] rs1;
    wire[4:0] rs2;
    wire[4:0] rd;
    wire[31:0] i_type_imm;
    wire[31:0] u_type_imm;
    wire[31:0] j_type_imm;
    wire[31:0] b_type_imm;
    wire[31:0] s_type_imm;
    reg[31:0] insn;

    // NOTE: Negedge shouldn't really be required here but verilog is
    // being annoying
    `define CHECK_INSN(INSN, VAL0, EXP0, VAL1, EXP1, VAL2, EXP2) \
        insn = INSN; \
        @(negedge clk); \
        `ASSERT_EQ(VAL0, EXP0); \
        `ASSERT_EQ(VAL1, EXP1); \
        `ASSERT_EQ(VAL2, EXP2);

    always @(posedge clk) begin
        `CHECK_INSN('h003100b3, rd, 1, rs1, 2, rs2, 3); // add ra, sp, gp

        `CHECK_INSN('h00a10093, rd, 1, rs1, 2, i_type_imm, 10); // addi ra,sp,10
        `CHECK_INSN('h7ff10093, rd, 1, rs1, 2, i_type_imm, 2047); // addi ra,sp,10
        `CHECK_INSN('h7ff10093, rd, 1, rs1, 2, i_type_imm, 2047); // addi a0,a1,2047
        `CHECK_INSN('hff658513, rd, 10, rs1, 11, i_type_imm, -10); // addi a0,a1,-10
        `CHECK_INSN('h80058513, rd, 10, rs1, 11, i_type_imm, -2048); // addi a0,a1,-2048

        `CHECK_INSN('h00001a37, rd, 20, u_type_imm, 'b010000_0000_0000, 0, 0); // lui s4,0x1
        `CHECK_INSN('hfffffa37, rd, 20, u_type_imm, 'hffff_f000, 0, 0); // lui x20, 1048575

        `CHECK_INSN('h00200a6f, rd, 20, j_type_imm, 2, 0, 0); // jal x20, 2
        `CHECK_INSN('hfffffa6f, rd, 20, j_type_imm, -2, 0, 0); // jal x20, -2
        // Bit set in imm[11]
        `CHECK_INSN('h00100a6f, rd, 20, j_type_imm, 'b100000000000, 0, 0); // jal x20, 0b100000000000
        // All bits set in imm[10:1]
        `CHECK_INSN('h3fe00a6f, rd, 20, j_type_imm, 'h3fe, 0, 0); // jal x20, 0x3fe
        // All bits set in imm[19:12]
        `CHECK_INSN('h07c00a6f, rd, 20, j_type_imm, 'h07c, 0, 0); // jal x20, 0x07c
        // Bit set in imm[20]. Should sign extend
        `CHECK_INSN('h80000a6f, rd, 20, j_type_imm, -1048576, 0, 0); // jal x20, -1048576
        // Max positive number
        `CHECK_INSN('h7ffffa6f, rd, 20, j_type_imm, 1048574, 0, 0); // jal x20, 1048574

        `CHECK_INSN('h00208163, rs1, 1, rs2, 2, b_type_imm, 2); // beq x1,x2,2
        `CHECK_INSN('hfe208fe3, rs1, 1, rs2, 2, b_type_imm, -2); // beq x1,x2,-2
        `CHECK_INSN('h7e208fe3, rs1, 1, rs2, 2, b_type_imm, 4094); // beq x1,x2,4094
        `CHECK_INSN('h80208063, rs1, 1, rs2, 2, b_type_imm, -4096); // beq x1,x2,4094
        // All ones in bit 1..4
        `CHECK_INSN('h00208f63, rs1, 1, rs2, 2, b_type_imm, 'b11110); // beq x1,x2,0b11110
        // All ones in bit 10..5
        `CHECK_INSN('h7e208063, rs1, 1, rs2, 2, b_type_imm, 'b11111100000); // beq ...
        // one in bit 11
        `CHECK_INSN('h002080e3, rs1, 1, rs2, 2, b_type_imm, 'b100000000000); // beq ...
        // one in bit 12 is tested by -4096

        `CHECK_INSN('h001120a3, rs1, 2, rs2, 1, s_type_imm, 1); // sw x1,1(x2)
        `CHECK_INSN('hfe20afa3, rs1, 1, rs2, 2, s_type_imm, -1); // sw x1,-1(x2)
        `CHECK_INSN('h7e20afa3, rs1, 1, rs2, 2, s_type_imm, 2047); // sw x1,2047(x2)
        `CHECK_INSN('h8020a023, rs1, 1, rs2, 2, s_type_imm, -2048); // sw x1,-2048(x2)
        // All ones in bit 0..4
        `CHECK_INSN('h0020afa3, rs1, 1, rs2, 2, s_type_imm, 'b11111); // sw x1,0b11111(x2)
        // All ones in bit 10..5
        `CHECK_INSN('h7e20a023, rs1, 1, rs2, 2, s_type_imm, 'b011111100000); // sw ..
        // Bit 11 is checked by -2048

        `END_TEST
    end

    e_decode_rs1 uut_rs1
        ( ._i_decode_insn(insn)
        , .__output(rs1)
        );

    e_decode_rs2 uut_rs2
        ( ._i_decode_insn(insn)
        , .__output(rs2)
        );

    e_decode_rd uut_rd
        ( ._i_decode_insn(insn)
        , .__output(rd)
        );

    e_decode_i_type_immediate uut_i_type
        ( ._i_decode_insn(insn)
        , .__output(i_type_imm)
        );

    e_decode_u_type_immediate uut_u_type
        ( ._i_decode_insn(insn)
        , .__output(u_type_imm)
        );

    e_decode_j_type_immediate uut_j_type
        ( ._i_decode_insn(insn)
        , .__output(j_type_imm)
        );

    e_decode_b_type_immediate uut_b_type
        ( ._i_decode_insn(insn)
        , .__output(b_type_imm)
        );

    e_decode_s_type_immediate uut_s_type
        ( ._i_decode_insn(insn)
        , .__output(s_type_imm)
        );
endmodule
