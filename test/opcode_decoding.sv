`include "test/vatch/main.v"

module opcode_decoding_tb;
    `SETUP_TEST
    `CLK_AND_RST(clk, rst, 1)

    wire[36:0] result;

    e_instructions_test_translate_opcode uut(.__output(result));

    initial begin
        // NOTE: Not sure why this delay is needed. Not a good sign...
        #1

        `ASSERT_EQ(result[0],  1, "Failed to decode LUI");
        `ASSERT_EQ(result[1],  1, "Failed to decode AUIPC");
        `ASSERT_EQ(result[2],  1, "Failed to decode JAL");
        `ASSERT_EQ(result[3],  1, "Failed to decode JALR");
        `ASSERT_EQ(result[4],  1, "Failed to decode BEQ");
        `ASSERT_EQ(result[5],  1, "Failed to decode BNE");
        `ASSERT_EQ(result[6],  1, "Failed to decode BLT");
        `ASSERT_EQ(result[7],  1, "Failed to decode BGE");
        `ASSERT_EQ(result[8],  1, "Failed to decode BLTU");
        `ASSERT_EQ(result[9],  1, "Failed to decode BGEU");
        `ASSERT_EQ(result[10], 1, "Failed to decode LB");
        `ASSERT_EQ(result[11], 1, "Failed to decode LH");
        `ASSERT_EQ(result[12], 1, "Failed to decode LW");
        `ASSERT_EQ(result[13], 1, "Failed to decode LBU");
        `ASSERT_EQ(result[14], 1, "Failed to decode LHU");
        `ASSERT_EQ(result[15], 1, "Failed to decode SB");
        `ASSERT_EQ(result[16], 1, "Failed to decode SH");
        `ASSERT_EQ(result[17], 1, "Failed to decode SW");
        `ASSERT_EQ(result[18], 1, "Failed to decode ADDI");
        `ASSERT_EQ(result[19], 1, "Failed to decode SLTI");
        `ASSERT_EQ(result[20], 1, "Failed to decode SLTIU");
        `ASSERT_EQ(result[21], 1, "Failed to decode XORI");
        `ASSERT_EQ(result[22], 1, "Failed to decode ORI");
        `ASSERT_EQ(result[23], 1, "Failed to decode ANDI");
        `ASSERT_EQ(result[24], 1, "Failed to decode SLLI");
        `ASSERT_EQ(result[25], 1, "Failed to decode SRLI");
        `ASSERT_EQ(result[26], 1, "Failed to decode SRAI");
        `ASSERT_EQ(result[27], 1, "Failed to decode ADD");
        `ASSERT_EQ(result[28], 1, "Failed to decode SUB");
        `ASSERT_EQ(result[29], 1, "Failed to decode SLL");
        `ASSERT_EQ(result[30], 1, "Failed to decode SLT");
        `ASSERT_EQ(result[31], 1, "Failed to decode SLTU");
        `ASSERT_EQ(result[32], 1, "Failed to decode XOR");
        `ASSERT_EQ(result[33], 1, "Failed to decode SRL");
        `ASSERT_EQ(result[34], 1, "Failed to decode SRA");
        `ASSERT_EQ(result[35], 1, "Failed to decode OR");
        `ASSERT_EQ(result[36], 1, "Failed to decode AND");

        `END_TEST
    end

endmodule
