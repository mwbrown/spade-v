`include "test/vatch/main.v"

module memory_tb();
    `SETUP_TEST
    `CLK_AND_RST(clk, rst, 1)

    reg[2:0] access_width;
    reg[31:0] addr;
    reg we;
    reg[31:0] data_in;
    wire[31:0] data_out;

    e_data_memory_internal uut(
        ._i_clk(clk),
        ._i_access_width(access_width),
        ._i_addr(addr),
        ._i_we(we),
        ._i_write_data(data_in),
        .__output(data_out)
    );

    localparam Full = 0;
    localparam HalfU = 1;
    localparam HalfS = 2;
    localparam ByteU = 3;
    localparam ByteS = 4;

    `define WRITE(W, ADDR, DATA) \
        access_width <= W; \
        addr <= ADDR; \
        data_in <= DATA; \
        we <= 1;

    `define READ(W, ADDR) \
        access_width <= W; \
        addr <= ADDR; \
        we <= 0;


    initial begin
        @(negedge rst);

        @(negedge clk)
        // Full width aligned writes and reads work
        `WRITE(Full, 0, 'h10203040);
        @(negedge clk)
        `WRITE(Full, 4, 11);
        @(negedge clk)
        `READ(Full, 0);
        @(negedge clk)
        `READ(Full, 4);
        `ASSERT_EQ(data_out, 'h10203040);
        @(negedge clk)
        `ASSERT_EQ(data_out, 11);

        // Unsigned byte reads work
        @(negedge clk)
        `READ(ByteS, 0)
        @(negedge clk)
        `ASSERT_EQ(data_out, 'h40);
        `READ(ByteS, 1)
        @(negedge clk)
        `ASSERT_EQ(data_out, 'h30);
        `READ(ByteS, 2)
        @(negedge clk)
        `ASSERT_EQ(data_out, 'h20);
        `READ(ByteS, 3)
        @(negedge clk)
        `ASSERT_EQ(data_out, 'h10);

        // Writing bytes does not mangle neighbouring bytes
        @(negedge clk)
        `WRITE(ByteS, 0, 'h7f);
        @(negedge clk)
        `WRITE(ByteS, 1, 'h6f);
        @(negedge clk)
        `READ(ByteS, 0)
        @(negedge clk)
        `ASSERT_EQ(data_out, 'h7f);
        `READ(ByteS, 1)
        @(negedge clk)
        `ASSERT_EQ(data_out, 'h6f);
        `READ(ByteS, 2)
        @(negedge clk)
        `ASSERT_EQ(data_out, 'h20);
        `READ(ByteS, 3)
        @(negedge clk)
        `ASSERT_EQ(data_out, 'h10);



        // Byte writes work and only overwrite target byte
        @(negedge clk)
        `WRITE(Full, 0, 'hffff_ffff);
        @(negedge clk)
        `WRITE(ByteS, 3, 'h20);
        @(negedge clk)
        `WRITE(ByteS, 2, 'h01);
        @(negedge clk)
        `READ(Full, 0);
        @(negedge clk)
        `ASSERT_EQ(data_out, 'h2001ffff);

        // Half words work
        @(negedge clk)
        `WRITE(HalfS, 0, 'h1234);
        @(negedge clk)
        `WRITE(HalfS, 2, 'h5678);
        // Unaligned
        @(negedge clk)
        `WRITE(HalfS, 5, 'h7bcd);
        @(negedge clk)
        `READ(HalfS, 0)
        @(negedge clk)
        `ASSERT_EQ(data_out, 'h1234)
        `READ(HalfS, 2)
        @(negedge clk)
        `ASSERT_EQ(data_out, 'h5678)
        `READ(HalfS, 5)
        @(negedge clk)
        `ASSERT_EQ(data_out, 'h7bcd)

        // Signed bytes and half words are sign extended
        @(negedge clk)
        `WRITE(ByteS, 0, 'h80);
        @(negedge clk)
        `WRITE(HalfS, 3, 'h8000);
        @(negedge clk)
        `READ(ByteS, 0);
        @(negedge clk)
        `ASSERT_EQ(data_out, 'hffffff80)
        `READ(HalfS, 3)
        @(negedge clk)
        `ASSERT_EQ(data_out, 'hffff8000)

        // Unsigned bytes and half words are not sign extended
        @(negedge clk)
        `READ(ByteU, 0);
        @(negedge clk)
        `ASSERT_EQ(data_out, 'h80)
        `READ(HalfU, 3)
        @(negedge clk)
        `ASSERT_EQ(data_out, 'h8000)

        #50

        @(negedge clk);
        `READ(Full, 0);
        @(negedge clk)
        $display("00: %h", data_out);
        `READ(Full, 4);
        @(negedge clk)
        $display("04: %h", data_out);


        #100;

        `END_TEST
    end

endmodule
