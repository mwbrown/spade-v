#     li t1,1
#     li t2,2048
#     nop
#     nop
#     sb t1,0(t2)
# hold:
#     j hold
#     nop
#     nop
#     nop


    li t0, 300000 # Loop count (1 s at 12 MHz)
    li t1, 0 # LED state
    li t2, 2048 # LED peripheral base
loop:
    addi t0,t0,-1
    beq zero, t0, timeout # NOTE: Not sure if I support bne yet, let's use eq for now
    j loop
timeout:
    # Clear loop count
    li t0, 300000

    # Update LED
    beq zero,t1,was_off
    li t1, 0
    j led_write
was_off:
    li t1, 1
led_write:
    # Write to LED periphreal
    # li t1, 2
    nop
    nop
    nop
    sb t1,0(t2)

    j loop
